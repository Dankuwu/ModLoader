namespace SteamQueries.Models
{
    public class DownloadRequest : IMessage
    {
        public string MessageType { get; set; } = nameof(DownloadRequest);
        public ulong PublishFieldId { get; set; }
    }
}