﻿namespace SteamQueries.Models
{
    public class GetSubscribedItemsRequest : IMessage
    {
        public int Page { get; set; }
        public string MessageType { get; set; } = nameof(GetSubscribedItemsRequest);
    }
}
