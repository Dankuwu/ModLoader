﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mod_Manager.Abstractions;
using Mod_Manager.Models;

namespace Mod_Manager.Utilities;

internal class Process : IProcess
{
    private readonly ISettings _settings;

    public Process(ISettings settings)
    {
        _settings = settings;
    }

    public void Start(string fileName) => System.Diagnostics.Process.Start(fileName);
    public void Start(string fileName, string arguments) => System.Diagnostics.Process.Start(fileName, arguments);
    public void Start(ProcessStartInfo startInfo) => System.Diagnostics.Process.Start(startInfo);
    public System.Diagnostics.Process[] GetProcessesByName(string? processName) => System.Diagnostics.Process.GetProcessesByName(processName);
    
    public async Task<System.Diagnostics.Process?> StartVtolVr()
    {
        var args = new StringBuilder("-applaunch 667970 --doorstop-enabled true");

        switch (_settings.GetVRMode())
        {
            case Settings.VRMode.SteamVR:
                break;
            case Settings.VRMode.Oculus:
                args.Append(" oculus");
                break;
            case Settings.VRMode.OpenXR:
                args.Append(" openxr");
                break;
        }
        
        var steam = GetProcessesByName("steam");
        var steamPath = steam.First().MainModule.FileName;
        var processInfo = new ProcessStartInfo(steamPath, args.ToString());
        System.Diagnostics.Process.Start(processInfo);
        await Task.Delay(TimeSpan.FromSeconds(3));

        var vtolProcesses = GetProcessesByName("vtolvr");
        return !vtolProcesses.Any() ? null : vtolProcesses.First();
    }
}