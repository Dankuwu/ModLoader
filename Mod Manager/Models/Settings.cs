﻿using Facepunch.Steamworks.Abstractions;
using Mod_Manager.Abstractions;
using Newtonsoft.Json;

namespace Mod_Manager.Models;

internal sealed class Settings: ISettings
{
    private const string fileName = "settings.json";
    private ISteamRemoteStorage _steamStorage;

    public VRMode GetVRMode()
    {
        var settings = LoadSettings();
        return settings.VRMode;
    }

    public void SetVRMode(VRMode newValue)
    {
        var settings = LoadSettings();
        settings.VRMode = newValue;
        SaveSettings(settings);
    }

    public Settings(ISteamRemoteStorage steamStorage)
    {
        _steamStorage = steamStorage;
    }

    private SettingsJson LoadSettings()
    {
        if (!_steamStorage.FileExists(fileName))
        {
            return new SettingsJson();
        }

        var fileText = _steamStorage.FileReadText(fileName);
        var convertedObject = JsonConvert.DeserializeObject<SettingsJson>(fileText);
        return convertedObject;
    }

    private void SaveSettings(SettingsJson settingsClass)
    {
        var jsonText = JsonConvert.SerializeObject(settingsClass, Formatting.Indented);
        _steamStorage.FileWrite(fileName, jsonText);
    }
    
    public enum VRMode
    {
        SteamVR,
        Oculus,
        OpenXR
    }

    private class SettingsJson
    {
        [JsonProperty]
        public VRMode VRMode;
    }
}