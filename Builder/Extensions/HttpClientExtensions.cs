using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Builder.Extensions;

static class HttpClientExtensions
{
    public static async Task DownloadFile(this HttpClient client, string address, string fileName)
    {
        using var response = await client.GetAsync(address);
        await using var stream = await response.Content.ReadAsStreamAsync();
        await using var file = File.OpenWrite(fileName);
        await stream.CopyToAsync(file);
    }
}