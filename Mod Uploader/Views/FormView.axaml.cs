﻿using System.Linq;
using System.Reactive;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Platform.Storage;
using Avalonia.ReactiveUI;
using Mod_Uploader.ViewModels;
using ReactiveUI;

namespace Mod_Uploader.Views;

public partial class FormView : ReactiveUserControl<FormViewModel>
{
    public FormView()
    {
        InitializeComponent();
        this.WhenActivated(d => d(ViewModel.ShowSelectFolderDialog.RegisterHandler(ShowSelectFolderDialog)));
        this.WhenActivated(d => d(ViewModel.ShowSelectThumbnailDialog.RegisterHandler(ShowSelectThumbnailDialog)));
    }

    private async Task ShowSelectThumbnailDialog(InteractionContext<Unit, string?> interaction)
    {
        var mainWindow = TopLevel.GetTopLevel(this);
        if (mainWindow == null)
        {
            interaction.SetOutput(null);
            return;
        }
        
        var folder = await mainWindow.StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions
        {
            AllowMultiple = false,
            Title = "Select a thumbnail", 
            FileTypeFilter = new []{ FilePickerFileTypes.ImageAll }
        });

        var pathAbsolutePath = folder.FirstOrDefault()?.TryGetLocalPath();
        interaction.SetOutput(pathAbsolutePath);
    }

    private async Task ShowSelectFolderDialog(InteractionContext<Unit, string?> interaction)
    {
        var mainWindow = TopLevel.GetTopLevel(this);
        if (mainWindow == null)
        {
            interaction.SetOutput(null);
            return;
        }
        
        var folder = await mainWindow.StorageProvider.OpenFolderPickerAsync(new FolderPickerOpenOptions
        {
            AllowMultiple = false,
            Title = "Select a folder to upload"
        });

        var pathAbsolutePath = folder.FirstOrDefault()?.TryGetLocalPath();
        interaction.SetOutput(pathAbsolutePath);
    }
}