﻿namespace Mod_Uploader.Models;

public enum SteamVisibility
{
    Public,
    FriendsOnly,
    Hidden,
    Unlisted
}