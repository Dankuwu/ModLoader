﻿using Facepunch.Steamworks.Abstractions;

namespace Mod_Uploader.Abstractions;

public interface IContext
{
    event SteamItemScopeChanged SteamItemScopeChanged;

    void ChangeSteamItemScope(object sender, IItem? newSteamItem);

    IItem? GetSteamItemScope();
}

public delegate void SteamItemScopeChanged(object sender, IItem? item);