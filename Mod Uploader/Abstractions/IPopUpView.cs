﻿using System.Threading.Tasks;
using Mod_Uploader.Models;

namespace Mod_Uploader.Abstractions;

public interface IPopUpView: IViewModel
{
    Task<PopUpButton> ShowPopUp(string title, string message, string? leftButtonText = null, string? rightButtonText = null);
}