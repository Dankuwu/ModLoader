﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Facepunch.Steamworks.Abstractions;
using Facepunch.Steamworks.Abstractions.Wrappers;
using Mod_Uploader.Abstractions;
using Mod_Uploader.Models;
using Mod_Uploader.Utilities;
using Newtonsoft.Json;
using ReactiveUI;
using Serilog;
using SteamQueries.Models;
using Process = Mod_Uploader.Utilities.Process;

namespace Mod_Uploader.ViewModels;

public class FormViewModel : ViewModelBase, IFormViewModel
{
    private const bool _allowLoadOnStartDefault = true;
    private const bool _showOnMainMenuDefault = true;
    private const string _imagePathDefault = "No Change";
    private const string _folderPathDefault = "No New Files";
    private bool _canUpload;
    public bool CanUpload
    {
        get => _canUpload;
        set => this.RaiseAndSetIfChanged(ref _canUpload, value); 
    }
    
    private string _title = "My Amazing Title";
    public string Title
    {
        get => _title;
        set
        {
            this.RaiseAndSetIfChanged(ref _title, value);
            ValidateForm();
        }
    }
    
    private string _description = "My Amazing Description";
    public string Description
    {
        get => _description;
        set
        {
            this.RaiseAndSetIfChanged(ref _description, value);
            ValidateForm();
        }
    }
    
    private bool _descriptionIsInvalid;
    public bool DescriptionIsInvalid
    {
        get => _descriptionIsInvalid;
        set => this.RaiseAndSetIfChanged(ref _descriptionIsInvalid, value); 
    }
    
    private string _descriptionErrorText;
    public string DescriptionErrorText
    {
        get => _descriptionErrorText;
        set => this.RaiseAndSetIfChanged(ref _descriptionErrorText, value); 
    }
    
    private bool _titleIsInvalid;
    public bool TitleIsInvalid
    {
        get => _titleIsInvalid;
        set => this.RaiseAndSetIfChanged(ref _titleIsInvalid, value); 
    }
    
    private string _titleErrorText;
    public string TitleErrorText
    {
        get => _titleErrorText;
        set => this.RaiseAndSetIfChanged(ref _titleErrorText, value); 
    }
    
    private bool _showOnMainMenu = _showOnMainMenuDefault;
    public bool ShowOnMainMenu
    {
        get => _showOnMainMenu;
        set => this.RaiseAndSetIfChanged(ref _showOnMainMenu, value); 
    }
    
    private bool _allowLoadOnStart = _allowLoadOnStartDefault;
    public bool AllowLoadOnStart
    {
        get => _allowLoadOnStart;
        set => this.RaiseAndSetIfChanged(ref _allowLoadOnStart, value); 
    }
    
    private string _folderPath;
    public string FolderPath
    {
        get => _folderPath;
        set => this.RaiseAndSetIfChanged(ref _folderPath, value); 
    }
    
    private bool _folderPathIsInvalid;
    public bool FolderPathIsInvalid
    {
        get => _folderPathIsInvalid;
        set => this.RaiseAndSetIfChanged(ref _folderPathIsInvalid, value); 
    }
    
    private string _folderPathErrorText;
    public string FolderPathErrorText
    {
        get => _folderPathErrorText;
        set => this.RaiseAndSetIfChanged(ref _folderPathErrorText, value); 
    }
    
    private string _imagePath;
    public string ImagePath
    {
        get => _imagePath;
        set => this.RaiseAndSetIfChanged(ref _imagePath, value); 
    }
    
    private bool _imagePathIsInvalid;
    public bool ImagePathIsInvalid
    {
        get => _imagePathIsInvalid;
        set => this.RaiseAndSetIfChanged(ref _imagePathIsInvalid, value); 
    }
    
    private string _imagePathErrorText;
    public string ImagePathErrorText
    {
        get => _imagePathErrorText;
        set => this.RaiseAndSetIfChanged(ref _imagePathErrorText, value); 
    }

    private ObservableCollection<string> _files = new () { "No Change" };
    public ObservableCollection<string> Files
    {
        get => _files;
        set => this.RaiseAndSetIfChanged(ref _files, value); 
    }
    
    private int _filesSelectedIndex;
    public int FilesSelectedIndex
    {
        get => _filesSelectedIndex;
        set => this.RaiseAndSetIfChanged(ref _filesSelectedIndex, value); 
    }

    public List<SteamVisibility> Visibilities { get; set; } = new()
    {
        SteamVisibility.Public, SteamVisibility.FriendsOnly, SteamVisibility.Hidden, SteamVisibility.Unlisted
    };
    
    private int _visibilitySelectedIndex;
    public int VisibilitySelectedIndex
    {
        get => _visibilitySelectedIndex;
        set => this.RaiseAndSetIfChanged(ref _visibilitySelectedIndex, value); 
    }

    public List<WorkshopTag> WorkshopTags { get; set; }
    
    public SelectedTags SelectedTags { get; }
    
    public ReactiveCommand<Unit, Unit> SelectFolderCommand { get; set; }
    public Interaction<Unit, string?> ShowSelectFolderDialog { get; set; }
    public ReactiveCommand<Unit, Unit> SelectThumbnailCommand { get; set; }
    public Interaction<Unit, string?> ShowSelectThumbnailDialog { get; set; }
    
    public ReactiveCommand<Unit, Unit> UploadCommand { get; set; }

    private IContext _context;
    private readonly IFileSystem _fileSystem;
    private readonly IEditorFactory _editorFactory;
    private readonly IMD5 _md5;
    private readonly IPopUpView _popUpView;
    private readonly IProcess _process;

    public FormViewModel() : this(new Context(), new MockFileSystem(), new EditorFactory(), new MD5Wrapped(), null, new Process())
    {
        FolderPath = "C:\\Users\\UserName\\Documents\\MyMod\\Bin";
        ImagePath = "C:\\Users\\UserName\\Pictures\\MyModImage.png";
    }

    public FormViewModel(IContext context, IFileSystem fileSystem, IEditorFactory editorFactory, IMD5 md5, IPopUpView popUpView, IProcess process)
    {
        _context = context;
        _fileSystem = fileSystem;
        _editorFactory = editorFactory;
        _md5 = md5;
        _popUpView = popUpView;
        _process = process;
        ValidateForm();

        _context.SteamItemScopeChanged += OnSteamItemScopeChanged;
        SelectFolderCommand = ReactiveCommand.CreateFromTask(SelectFolderPressed);
        SelectThumbnailCommand = ReactiveCommand.CreateFromTask(SelectThumbnailPressed);
        UploadCommand = ReactiveCommand.CreateFromTask(UploadButtonPressed);
        ShowSelectFolderDialog = new Interaction<Unit, string?>();
        ShowSelectThumbnailDialog = new Interaction<Unit, string?>();
        SelectedTags = new SelectedTags();
        WorkshopTags = new List<WorkshopTag>
        {
            new ("Vehicle", WorkshopTagChanged),
            new ("Helmet", WorkshopTagChanged),
            new ("Weapon", WorkshopTagChanged),
            new ( "Sound", WorkshopTagChanged),
            new ("Multiplayer Compatible", WorkshopTagChanged)
        };
    }
    
    public ViewModelBase GetViewModel() => this;

    private void OnSteamItemScopeChanged(object sender, IItem? steamItem)
    {
        if (steamItem == null)
        {
            Title = string.Empty;
            Description = string.Empty;
            ImagePath = string.Empty;
            FolderPath = string.Empty;
            AllowLoadOnStart = _allowLoadOnStartDefault;
            ShowOnMainMenu = _showOnMainMenuDefault;
            VisibilitySelectedIndex = 0;
            SelectedTags.Clear();
            return;
        }
        
        Title = steamItem.Title;
        Description = steamItem.Description;
        ImagePath = _imagePathDefault;
        FolderPath = _folderPathDefault;

        if (steamItem.IsPublic)
        {
            VisibilitySelectedIndex = (int)SteamVisibility.Public;
        }
        else if (steamItem.IsFriendsOnly)
        {
            VisibilitySelectedIndex = (int)SteamVisibility.FriendsOnly;
        }
        else if (steamItem.IsUnlisted)
        {
            VisibilitySelectedIndex = (int)SteamVisibility.Unlisted;
        }
        else if (steamItem.IsPrivate)
        {
            VisibilitySelectedIndex = (int)SteamVisibility.Hidden;
        }

        foreach (var tag in steamItem.Tags)
        {
            var existingTag = WorkshopTags.FirstOrDefault(t => t.Name.Equals(tag, StringComparison.OrdinalIgnoreCase));
            if (existingTag == null)
            {
                var workshopTag = new WorkshopTag(tag, WorkshopTagChanged) { IsChecked = true };
                WorkshopTags.Add(workshopTag);
                SelectedTags.Add(workshopTag);
                continue;
            }
            
            SelectedTags.Add(existingTag);
            existingTag.IsChecked = true;
        }

        if (string.IsNullOrEmpty(steamItem.MetaData))
        {
            return;
        }
        
        var itemMetaData = default(ItemMetaData);
        try
        {
            itemMetaData = JsonConvert.DeserializeObject<ItemMetaData>(steamItem.MetaData);
        }
        catch (Exception e)
        {
            return;
        }

        AllowLoadOnStart = itemMetaData.AllowLoadOnStart;
        ShowOnMainMenu = itemMetaData.ShowOnMainList;
    }

    private void ValidateForm()
    {
        CanUpload = true;
        
        TitleValidation();
        FolderValidation();
        ImageValidation();
        DescriptionValidation();
        Log.Verbose(CanUpload ? "The form is now valid" : "The form is not valid");
    }

    private void TitleValidation()
    {
        if (string.IsNullOrWhiteSpace(Title))
        {
            TitleErrorText = "Title is required";
            TitleIsInvalid = true;
            CanUpload = false;
            return;
        }

        if (Title.Equals("My Amazing Title"))
        {
            TitleErrorText = "Title needs to be set";
            TitleIsInvalid = true;
            CanUpload = false;
            return;
        }

        TitleIsInvalid = false;
    }

    private void FolderValidation()
    {
        var isCreatingNewItem = _context.GetSteamItemScope() == null;
        
        if (isCreatingNewItem && string.IsNullOrEmpty(FolderPath))
        {
            FolderPathIsInvalid = true;
            FolderPathErrorText = "Upload Folder Path can't be empty";
            CanUpload = false;
            return;
        }

        FolderPathIsInvalid = false;
    }

    private void ImageValidation()
    {
        var isCreatingNewItem = _context.GetSteamItemScope() == null;

        if (isCreatingNewItem && string.IsNullOrEmpty(ImagePath))
        {
            ImagePathIsInvalid = true;
            CanUpload = false;
            ImagePathErrorText = "Thumbnail can't be blank";
            return;
        }

        if (_fileSystem.File.Exists(ImagePath))
        {
            var fileInfo = _fileSystem.FileInfo.New(ImagePath);

            if (fileInfo.Length >= 1048576)
            {
                ImagePathIsInvalid = true;
                CanUpload = false;
                ImagePathErrorText = "Thumbnail can't be larger than 1MB";
                return;
            }
        }
        ImagePathIsInvalid = false;
    }

    private void DescriptionValidation()
    {
        if (string.IsNullOrWhiteSpace(Description))
        {
            CanUpload = false;
            DescriptionIsInvalid = true;
            DescriptionErrorText = "Description can't be empty";
            return;
        }

        DescriptionIsInvalid = false;
    }

    private async Task SelectFolderPressed()
    {
        var folder = await ShowSelectFolderDialog.Handle(Unit.Default);

        if (folder == null)
        {
            // User could of canceled
            return;
        }

        FolderPath = folder;
        UpdateDllDropdown();
        ValidateForm();
    }
    
    private async Task SelectThumbnailPressed()
    {
        var image = await ShowSelectThumbnailDialog.Handle(Unit.Default);

        if (image == null)
        {
            return;
        }

        ImagePath = image;
        ValidateForm();
    }

    private void UpdateDllDropdown()
    {
        var isCreatingNewItem = _context.GetSteamItemScope() == null;
        var directory = _fileSystem.DirectoryInfo.New(FolderPath);

        var files = directory.GetFiles();
        Files.Clear();
        
        if (isCreatingNewItem)
        {
            Files.Add("No Dll");
        }
        else
        {
            try
            {
                var metadata = _context.GetSteamItemScope().GetMetaData();
                Files.Add($"No Change. \"{metadata.DllName}\"");
            }
            catch (Exception e)
            {
                // Log fail
            }
        }
        
        foreach (var fileInfo in files)
        {
            Files.Add(fileInfo.Name);
        }
        
        FilesSelectedIndex = 0;
    }

    private async Task UploadButtonPressed()
    {
        Log.Information("Upload button was pressed");
        var isCreatingNewItem = _context.GetSteamItemScope() == null;

        if (isCreatingNewItem)
        {
            Log.Verbose("Creating new item");
            await CreateNewItem();
            return;
        }
        Log.Verbose("Editing existing item");

        await EditExistingItem();
    }

    private async Task CreateNewItem()
    {
        var metaData = await CreateNewMetaData();
        var metaDataText = JsonConvert.SerializeObject(metaData);

        Log.Verbose("Creating editor factory");
        var editor = _editorFactory.New(WorkshopFileType.Community)
            .ForAppId(App.LoaderAppId)
            .WithTitle(Title)
            .WithDescription(Description)
            .WithMetaData(metaDataText)
            .WithContent(FolderPath)
            .WithPreviewFile(ImagePath);

        Log.Verbose("Setting visibility");
        var visibility = Visibilities[VisibilitySelectedIndex];
        switch (visibility)
        {
            case SteamVisibility.Public:
                editor.WithPublicVisibility();
                break;
            case SteamVisibility.FriendsOnly:
                editor.WithFriendsOnlyVisibility();
                break;
            case SteamVisibility.Hidden:
                editor.WithPrivateVisibility();
                break;
            case SteamVisibility.Unlisted:
                editor.WithUnlistedVisibility();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        foreach (var workshopTag in SelectedTags.Tags)
        {
            editor.WithTag(workshopTag.Name);
        }
        
        Log.Information("Creating a new Steam Item");
        var result = await editor.SubmitAsync();

        if (!result.Success)
        {
            const string errorTitle = "Failed";
            if (result.NeedsWorkshopAgreement)
            {
                Log.Warning("Failed to create steam item because user has not agreed to steam workshop tos");
                await _popUpView.ShowPopUp(errorTitle, "You need to agree to steams workshops terms of service",
                    rightButtonText: "Okay");
                return;
            }
            
            Log.Warning("Failed to create steam item with reason of {SteamFailReason}", result.Result);
            await _popUpView.ShowPopUp(errorTitle, $"Reason:{result.Result}", rightButtonText: "Okay");
            return;
        }

        var response = await _popUpView.ShowPopUp("Uploaded!", "Your item has been uploaded to the steam workshop",
            "View In Steam", "Okay");

        if (response == PopUpButton.Left)
        {
            Log.Information("Uploaded to steam with the ID of {SteamId}", result.FileId.Value);
            string url =
                $"https://steamcommunity.com/sharedfiles/filedetails/?source=vtolvr-mod-loader&id={result.FileId.Value}";
            var escapedString = url.Replace("&", "^&");
            _process.Start(
                new ProcessStartInfo("cmd", $"/c start steam://openurl/{escapedString}") { CreateNoWindow = true });
        }
    }

    private async Task<ItemMetaData> CreateNewMetaData()
    {
        Log.Verbose("Creating new metadata");
        var metaData = new ItemMetaData { AllowLoadOnStart = AllowLoadOnStart, ShowOnMainList = ShowOnMainMenu };

        if (FilesSelectedIndex != 0)
        {
            var selectedDll = Files[FilesSelectedIndex];
            metaData.DllName = selectedDll;

            await using var stream = _fileSystem.File.OpenRead(Path.Combine(FolderPath, selectedDll));
            var hash = _md5.ComputeHash(stream);

            metaData.DllHash = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
        }
        else if (_context.GetSteamItemScope() != null)
        {
            var oldMetaData = JsonConvert.DeserializeObject<ItemMetaData>(_context.GetSteamItemScope().MetaData);
            if (string.IsNullOrEmpty(oldMetaData.DllName))
            {
                return metaData;
            }

            metaData.DllName = oldMetaData.DllName;

            var path = Path.Combine(FolderPath, metaData.DllName);
            if (_fileSystem.File.Exists(path))
            {
                await using var stream = _fileSystem.File.OpenRead(path);
                var hash = _md5.ComputeHash(stream);

                metaData.DllHash = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            }
            else
            {
                Log.Warning("User said not to change the DLL however the DLL was did not exist at {ExpectedPath}", path);
            }
        }

        return metaData;
    }

    private async Task EditExistingItem()
    {
        var item = _context.GetSteamItemScope();
        Log.Information("Updating existing steam item with the ID of {SteamId}", item?.Id?.Value);
        var editor = _editorFactory.New(item.Id);
        editor.ForAppId(App.LoaderAppId);
        
        if (!item.Title.Equals(Title))
        {
            editor.WithTitle(Title);
        }

        if (!FolderPath.Equals(_folderPathDefault))
        {
            editor.WithContent(FolderPath);
        }

        if (!ImagePath.Equals(_imagePathDefault))
        {
            editor.WithPreviewFile(ImagePath);
        }

        if (!item.Description.Equals(Description))
        {
            editor.WithDescription(Description);
        }
        
        var visibility = Visibilities[VisibilitySelectedIndex];
        Log.Verbose("Marking steam item visibility as {SteamItemVisibility}", Enum.GetName(visibility));
        switch (visibility)
        {
            case SteamVisibility.Public:
                editor.WithPublicVisibility();
                break;
            case SteamVisibility.FriendsOnly:
                editor.WithFriendsOnlyVisibility();
                break;
            case SteamVisibility.Hidden:
                editor.WithPrivateVisibility();
                break;
            case SteamVisibility.Unlisted:
                editor.WithUnlistedVisibility();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        foreach (var workshopTag in SelectedTags.Tags)
        {
            editor.WithTag(workshopTag.Name);
        }

        var metaData = string.IsNullOrEmpty(item.MetaData) ? null : item.GetMetaData();
        var newMetaData = await CreateNewMetaData();

        if (!Equals(metaData,newMetaData))
        {
            var metaDataText = JsonConvert.SerializeObject(newMetaData);
            editor.WithMetaData(metaDataText);
            Log.Verbose("The metadata changed to {MetadataInJson}", metaDataText);
        }
        
        Log.Information("Submitting changes to steam");
        var result = await editor.SubmitAsync();
        if (!result.Success)
        {
            const string errorTitle = "Failed";
            if (result.NeedsWorkshopAgreement)
            {
                Log.Warning("Failed to create steam item because user has not agreed to steam workshop tos");
                await _popUpView.ShowPopUp(errorTitle, "You need to agree to steams workshops terms of service",
                    rightButtonText: "Okay");
                return;
            }

            Log.Warning("Failed to create steam item with reason of {SteamFailReason}", result.Result);
            await _popUpView.ShowPopUp(errorTitle, $"Reason:{result.Result}", rightButtonText: "Okay");
            return;
        }

        var response = await _popUpView.ShowPopUp("Uploaded!", "Your item has been updated",
            "View In Steam", "Okay");

        if (response == PopUpButton.Left)
        {
            Log.Information("Updated the steam with the ID of {SteamId}", result.FileId.Value);
            string url =
                $"https://steamcommunity.com/sharedfiles/filedetails/?source=vtolvr-mod-loader&id={result.FileId.Value}";
            var escapedString = url.Replace("&", "^&");
            _process.Start(
                new ProcessStartInfo("cmd", $"/c start steam://openurl/{escapedString}") { CreateNoWindow = true });
        }
    }

    private void WorkshopTagChanged(WorkshopTag tag, bool newValue)
    {
        if (newValue)
        {
            SelectedTags.Add(tag);
        }
        else
        {
            SelectedTags.Remove(tag);
        }
    }
}