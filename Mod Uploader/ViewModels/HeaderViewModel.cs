﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Threading.Tasks;
using Facepunch.Steamworks.Abstractions;
using Facepunch.Steamworks.Abstractions.Wrappers;
using Mod_Uploader.Abstractions;
using Mod_Uploader.Models;
using ReactiveUI;
using Serilog;
using Process = Mod_Uploader.Utilities.Process;

namespace Mod_Uploader.ViewModels;

public class HeaderViewModel : ViewModelBase, IHeaderViewModel
{
    private const string _newSteamItemTitle = "Creating New Workshop Item";
    private const string _editingSteamItemTitle = "Editing: ";
    public ViewModelBase GetViewModel() => this;
    private readonly IContext _context;
    private readonly IProcess _process;
    private readonly IQuery _query;
    private readonly IFileSystem _fileSystem;
    public ReactiveCommand<Unit, Unit> ViewInSteamCommand { get; }
    public ReactiveCommand<IItem, Unit> OpenSteamItemCommand { get; }
    public ReactiveCommand<Unit, Unit> OpenModsFolderCommand { get; }
    public ReactiveCommand<Unit, Unit> NewSteamItemCommand { get; }
    private bool _canPressViewInSteam;
    public bool CanPressViewInSteam
    {
        get => _canPressViewInSteam;
        set => this.RaiseAndSetIfChanged(ref _canPressViewInSteam, value); 
    }

    public ObservableCollection<IItem> CreatedItems { get; set; } = new();
    private string _titleText;
    public string TitleText
    {
        get => _titleText;
        set => this.RaiseAndSetIfChanged(ref _titleText, value); 
    }

    public HeaderViewModel(IContext context, IProcess process, IQuery query, IScheduler scheduler, IFileSystem fileSystem)
    {
        _context = context;
        _process = process;
        _query = query;
        _fileSystem = fileSystem;
        ViewInSteamCommand = ReactiveCommand.Create(ViewInSteamPressed, outputScheduler: scheduler);
        OpenSteamItemCommand = ReactiveCommand.Create<IItem>(OnOpenSteamItem, outputScheduler: scheduler);
        NewSteamItemCommand = ReactiveCommand.Create(OnNewSteamItemPressed, outputScheduler: scheduler);
        OpenModsFolderCommand = ReactiveCommand.Create(OpenModsFolder, outputScheduler: scheduler);
        
        _context.SteamItemScopeChanged += OnSteamItemScopeChanged;
        CanPressViewInSteam = _context.GetSteamItemScope() != null;
    }

    private void OnNewSteamItemPressed()
    {
        _context.ChangeSteamItemScope(this, null);
    }

    private void OnSteamItemScopeChanged(object sender, IItem? steamItem)
    {
        CanPressViewInSteam = steamItem != null;
        UpdateTitleText();
    }

    private void OpenModsFolder()
    {
        var currentFolder = _fileSystem.Directory.GetCurrentDirectory();
        var modLoaderFolder = _fileSystem.DirectoryInfo.New(currentFolder).Parent;

        var expectedPath = Path.Combine(modLoaderFolder.FullName, "Mods");
        _fileSystem.Directory.CreateDirectory(expectedPath);
        
        _process.Start(new ProcessStartInfo("explorer", expectedPath));
        Log.Debug("User opened mods folder in explorer");
    }

    private void ViewInSteamPressed()
    {
        var steamId = _context.GetSteamItemScope().Id.Value;
        string url = $"https://steamcommunity.com/sharedfiles/filedetails/?source=vtolvr-mod-loader&id={steamId}";
        var escapedString = url.Replace("&", "^&");
        _process.Start(new ProcessStartInfo("cmd", $"/c start steam://openurl/{escapedString}") { CreateNoWindow = true });
        Log.Debug("User opened {ItemID} in steam", steamId);
    }

    public void UpdateTitleText()
    {
        var item = _context.GetSteamItemScope();
        if (item == null)
        {
            TitleText = _newSteamItemTitle;
            return;
        }
        
        TitleText = _editingSteamItemTitle + item.Title;
    }

    public async Task FetchUsersItems()
    {
        CreatedItems.Clear();

        var page = 1;
        while (true)
        {
            var results = await _query
                .WhereUserPublished()
                .WithDifferentApp(App.LoaderAppId, App.UploaderAppId)
                .WithMetadata(true)
                .WithLongDescription(true)
                .GetPageAsync(page);

            if (results.ResultCount == 0)
            {
                break;
            }

            foreach (var item in results.Entries)
            {
                CreatedItems.Add(item);
            }

            page += 1;

            // Some fail safe of an unknown case.
            if (page >= 100)
            {
                Log.Warning("Hit fail safe for too many pages of items");
                break;
            }
        }
    }
    
    private void OnOpenSteamItem(IItem item)
    {
        _context.ChangeSteamItemScope(this, item);
    }

    /// <summary>
    /// For XAML Preview
    /// </summary>
    public HeaderViewModel() : this(new Context(), new Process(), new QueryWrapper(), NewThreadScheduler.Default, new MockFileSystem())
    {
        TitleText = _newSteamItemTitle;
    }
}