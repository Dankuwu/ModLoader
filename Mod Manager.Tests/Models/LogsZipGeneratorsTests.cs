using System.IO.Abstractions.TestingHelpers;
using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Text;
using Facepunch.Steamworks.Abstractions;
using Mod_Manager.Abstractions;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.Models;

public class LogsZipGeneratorsTests
{
    class a_logs_generator_can
    {
        [Test]
        public async Task fetches_games_log()
        {
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(_anyUser, _gameLogFolder, "Player.log"), new MockFileData("Any Game Logs")}
            });
            fileSystem.Directory.CreateDirectory(Path.Combine(_vtolDir, "@Mod Loader"));

            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(_vtolDir);

            var runtimeInfo = new Mock<IRuntimeInfo>();
            runtimeInfo.Setup(r => r.IsOSPlatform(OSPlatform.Linux))
                .Returns(false);
            runtimeInfo.Setup(r => r.IsOSPlatform(OSPlatform.Windows))
                .Returns(true);
            runtimeInfo.Setup(r => r.GetFolderPath(Environment.SpecialFolder.UserProfile))
                .Returns(Path.Combine(_anyUser));

            var generator = new LogsZipGenerator(runtimeInfo.Object, fileSystem, Mock.Of<ISteamRemoteStorage>(),
                fileManager.Object);

            var outputPath = await generator.CollectLogs();

            var zipStream = fileSystem.File.OpenRead(outputPath);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            zipArchive.GetEntry("Player.log").Should().NotBeNull();
        }

        [Test]
        public async Task fetches_doorstop_config()
        {
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(_vtolDir, "doorstop_config.ini"), new MockFileData("Doorstop Config Contents")}
            });
            fileSystem.Directory.CreateDirectory(Path.Combine(_vtolDir, "@Mod Loader"));

            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(_vtolDir);

            var runtimeInfo = new Mock<IRuntimeInfo>();
            runtimeInfo.Setup(r => r.IsOSPlatform(It.IsAny<OSPlatform>()))
                .Returns(false);

            var generator = new LogsZipGenerator(runtimeInfo.Object, fileSystem, Mock.Of<ISteamRemoteStorage>(),
                fileManager.Object);

            var outputPath = await generator.CollectLogs();

            var zipStream = fileSystem.File.OpenRead(outputPath);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            zipArchive.GetEntry("doorstop_config.ini").Should().NotBeNull();
        }

        [Test]
        public async Task fetches_load_on_start()
        {
            var fileSystem = new MockFileSystem();
            fileSystem.Directory.CreateDirectory(Path.Combine(_vtolDir, "@Mod Loader"));

            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(_vtolDir);

            var runtimeInfo = new Mock<IRuntimeInfo>();
            runtimeInfo.Setup(r => r.IsOSPlatform(It.IsAny<OSPlatform>()))
                .Returns(false);

            var steamRemoteStorage = new Mock<ISteamRemoteStorage>();
            steamRemoteStorage.Setup(storage => storage.FileExists(LoadOnStartManager.LoadOnStartFile))
                .Returns(true);
            steamRemoteStorage.Setup(storage => storage.FileRead(LoadOnStartManager.LoadOnStartFile))
                .Returns(Encoding.Default.GetBytes("SomeJson"));

            var generator = new LogsZipGenerator(runtimeInfo.Object, fileSystem, steamRemoteStorage.Object,
                fileManager.Object);

            var outputPath = await generator.CollectLogs();

            var zipStream = fileSystem.File.OpenRead(outputPath);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            zipArchive.GetEntry($"{LoadOnStartManager.LoadOnStartFile}.json").Should().NotBeNull();
        }

        [Test]
        public async Task fetches_mod_manager_logs()
        {
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(_vtolDir, @"@Mod Loader\Mod Manager\Logs\Log_123456789.txt"), new MockFileData("Log Text")}
            });
            fileSystem.Directory.CreateDirectory(Path.Combine(_vtolDir, "@Mod Loader"));

            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(_vtolDir);

            var runtimeInfo = new Mock<IRuntimeInfo>();
            runtimeInfo.Setup(r => r.IsOSPlatform(It.IsAny<OSPlatform>()))
                .Returns(false);

            var generator = new LogsZipGenerator(runtimeInfo.Object, fileSystem, Mock.Of<ISteamRemoteStorage>(),
                fileManager.Object);

            var outputPath = await generator.CollectLogs();

            var zipStream = fileSystem.File.OpenRead(outputPath);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            zipArchive.GetEntry("Mod Manager Logs/Log_123456789.txt").Should().NotBeNull();
        }
        
        [Test]
        public async Task fetches_mod_uploader_logs()
        {
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(_vtolDir, @"@Mod Loader\Mod Uploader\Logs\Log_123456789.txt"), new MockFileData("Log Text")}
            });
            fileSystem.Directory.CreateDirectory(Path.Combine(_vtolDir, "@Mod Loader"));

            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(_vtolDir);

            var runtimeInfo = new Mock<IRuntimeInfo>();
            runtimeInfo.Setup(r => r.IsOSPlatform(It.IsAny<OSPlatform>()))
                .Returns(false);

            var generator = new LogsZipGenerator(runtimeInfo.Object, fileSystem, Mock.Of<ISteamRemoteStorage>(),
                fileManager.Object);

            var outputPath = await generator.CollectLogs();

            var zipStream = fileSystem.File.OpenRead(outputPath);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            zipArchive.GetEntry("Mod Uploader Logs/Log_123456789.txt").Should().NotBeNull();
        }
        
        [Test]
        public async Task fetches_steam_queries_logs()
        {
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(_vtolDir, @"@Mod Loader\SteamQueries\Logs\Log_123456789.txt"), new MockFileData("Log Text")}
            });
            fileSystem.Directory.CreateDirectory(Path.Combine(_vtolDir, "@Mod Loader"));

            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(_vtolDir);

            var runtimeInfo = new Mock<IRuntimeInfo>();
            runtimeInfo.Setup(r => r.IsOSPlatform(It.IsAny<OSPlatform>()))
                .Returns(false);

            var generator = new LogsZipGenerator(runtimeInfo.Object, fileSystem, Mock.Of<ISteamRemoteStorage>(),
                fileManager.Object);

            var outputPath = await generator.CollectLogs();

            var zipStream = fileSystem.File.OpenRead(outputPath);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            zipArchive.GetEntry("SteamQueries Logs/Log_123456789.txt").Should().NotBeNull();
        }

        [Test]
        public async Task creates_info_file()
        {
            var fileSystem = new MockFileSystem();
            fileSystem.Directory.CreateDirectory(Path.Combine(_vtolDir, "@Mod Loader"));

            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(_vtolDir);

            var runtimeInfo = new Mock<IRuntimeInfo>();
            runtimeInfo.Setup(r => r.IsOSPlatform(It.IsAny<OSPlatform>()))
                .Returns(false);

            var generator = new LogsZipGenerator(runtimeInfo.Object, fileSystem, Mock.Of<ISteamRemoteStorage>(),
                fileManager.Object);

            var outputPath = await generator.CollectLogs();

            var zipStream = fileSystem.File.OpenRead(outputPath);
            var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Read);
            zipArchive.GetEntry("Info.txt").Should().NotBeNull();
        }
    }

    private const string _vtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
    private const string _anyUser = @"C:\Users\AnyUserName";
    private const string _gameLogFolder = @"AppData\LocalLow\Boundless Dynamics, LLC\VTOLVR";
}