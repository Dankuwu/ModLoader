using Material.Icons;
using Microsoft.Reactive.Testing;
using Mod_Manager.Abstractions;
using Mod_Manager.ViewModels;
using ReactiveUI.Testing;

namespace Mod_Manager.Tests.ViewModels;

public class ItemListViewModelTests
{
    [Category("Unit")]
    class a_item_list_view_model
    {
        [Test]
        public void get_view_model_returns_type_of_view_model()
        {
            var name = "item";
            var item = new ItemListViewModel(Mock.Of<IHttp>(), Mock.Of<ILoadOnStartManager>(), ref name, 1,
                "Test User",
                "http:/example.com/image.png", "http:/steam.com", false, false, 123);
            var inheritance = item.GetViewModel() is ViewModelBase;
            inheritance.Should().BeTrue();
        }

        [Test]
        public void when_open_in_steam_is_pressed_calls_http() => new TestScheduler().With(scheduler =>
        {
            var name = "item";
            var http = new Mock<IHttp>();
            var item = new ItemListViewModel(http.Object, Mock.Of<ILoadOnStartManager>(), ref name, 1,
                "Test User",
                "http:/example.com/image.png", "http:/steam.com", false, false, 123);

            item.OpenInSteam.Execute().Subscribe();
            
            http.Verify(h => h.OpenInSteam(It.IsAny<string>()), Times.Once);
        });

        [Test]
        public void when_los_is_not_allowed_makes_button_disabled()
        {
            var itemName = "Name";
            var item = new ItemListViewModel(Mock.Of<IHttp>(), Mock.Of<ILoadOnStartManager>(), ref itemName, 1,
                "Test User",
                "http:/example.com/image.png", "http:/steam.com", false, false, 123);
            item.LosAllowed.Should().BeFalse();
        }
        
        [Test]
        public void when_los_is_allowed_makes_button_enabled()
        {
            var itemName = "Name";
            var item = new ItemListViewModel(Mock.Of<IHttp>(), Mock.Of<ILoadOnStartManager>(), ref itemName, 1,
                "Test User",
                "http:/example.com/image.png", "http:/steam.com", false, true, 123);
            item.LosAllowed.Should().BeTrue();
        }

        class when_toggle_load_on_start_is_pressed
        {
            [Category("Unit")]
            class on_a_steam_item
            {
                [Test]
                public void updates_ui() => new TestScheduler().With(scheduler =>
                {
                    var loadOnStartManager = new Mock<ILoadOnStartManager>();
                    var itemName = "Name";
                    var item = new ItemListViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, ref itemName, 1,
                        "Test User",
                        "http:/example.com/image.png", "http:/steam.com", false, true, 123);

                    item.LosIcon.Should().Be(MaterialIconKind.Rocket);
                    item.ToggleLoadOnStart.Execute().Subscribe();
                    item.LosIcon.Should().Be(MaterialIconKind.RocketLaunch);
                });

                [Test]
                public void calls_load_on_start_manager() => new TestScheduler().With(scheduler =>
                {
                    var loadOnStartManager = new Mock<ILoadOnStartManager>();
                    var itemName = "Name";
                    var item = new ItemListViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, ref itemName, 1,
                        "Test User",
                        "http:/example.com/image.png", "http:/steam.com", false, true, 123);

                    item.ToggleLoadOnStart.Execute().Subscribe();
                    loadOnStartManager
                        .Verify(los => los.ChangeStateOnItem(It.IsAny<ulong>(), It.IsAny<bool>()), Times.Once);
                });
            }

            [Category("Unit")]
            class on_a_local_item
            {
                [Test]
                public void updates_ui() => new TestScheduler().With(scheduler =>
                {
                    var loadOnStartManager = new Mock<ILoadOnStartManager>();
                    var itemName = "Name";
                    var item = new ItemListViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, ref itemName, 1,
                        "Test User",
                        "http:/example.com/image.png", false, true, "folder");

                    item.LosIcon.Should().Be(MaterialIconKind.Rocket);
                    item.ToggleLoadOnStart.Execute().Subscribe();
                    item.LosIcon.Should().Be(MaterialIconKind.RocketLaunch);
                });

                [Test]
                public void calls_load_on_start_manager() => new TestScheduler().With(scheduler =>
                {
                    var loadOnStartManager = new Mock<ILoadOnStartManager>();
                    var itemName = "Name";
                    var item = new ItemListViewModel(Mock.Of<IHttp>(), loadOnStartManager.Object, ref itemName, 1,
                        "Test User",
                        "http:/example.com/image.png", true, true, "folder");

                    item.ToggleLoadOnStart.Execute().Subscribe();
                    loadOnStartManager
                        .Verify(los => los.ChangeStateOnItem(It.IsAny<string>(), It.IsAny<bool>()), Times.Once);
                });
            }
        }
    }
}