﻿using System;
using System.Diagnostics;
using System.IO.Abstractions.TestingHelpers;
using System.Linq;
using System.Threading.Tasks;
using Facepunch.Steamworks.Abstractions;
using Facepunch.Steamworks.Abstractions.Wrappers;
using FluentAssertions;
using Microsoft.Reactive.Testing;
using Mod_Uploader.Abstractions;
using Mod_Uploader.ViewModels;
using Moq;
using NUnit.Framework;
using ReactiveUI.Testing;
using Steamworks;

namespace Mod_Uploader.Tests.ViewModels;

public class HeaderViewModelTests
{
    [Category("Unit")]
    private class a_header_view_model
    {
        [Test]
        public void sets_context_to_null_when_pressing_new_item() => new TestScheduler().With(scheduler =>
        {
            var context = new Mock<IContext>();
            var viewModel = new HeaderViewModel(context.Object,
                Mock.Of<IProcess>(),
                Mock.Of<IQuery>(),
                scheduler,
                new MockFileSystem());

            viewModel.NewSteamItemCommand.Execute().Subscribe();

            context.Verify(c => c.ChangeSteamItemScope(viewModel, null));
        });

        [Test]
        public void updates_ui_if_steam_scope_changes() => new TestScheduler().With(scheduler =>
        {
            const string itemTitle = "AnyItem";
            var item = new Mock<IItem>();
            item.Setup(i => i.Title).Returns(itemTitle);
            
            var context = new Mock<IContext>();
            context.Setup(c => c.GetSteamItemScope()).Returns(item.Object);
            
            var viewModel = new HeaderViewModel(context.Object,
                Mock.Of<IProcess>(),
                Mock.Of<IQuery>(),
                scheduler,
                new MockFileSystem());
            
            context.Raise(c => c.SteamItemScopeChanged += null, this, item.Object);

            viewModel.CanPressViewInSteam.Should().BeTrue();
            viewModel.TitleText.Should().Be($"Editing: {itemTitle}");
        });

        [Test]
        public void when_opening_in_steam_starts_process() => new TestScheduler().With(scheduler =>
        {
            const ulong expectedId = 123456789;
            var item = new Mock<IItem>();
            item.Setup(i => i.Id).Returns(new PublishedFieldIdWrapper(expectedId));
            
            var context = new Mock<IContext>();
            context.Setup(c => c.GetSteamItemScope()).Returns(item.Object);

            var process = new Mock<IProcess>();

            var viewModel = new HeaderViewModel(context.Object,
                process.Object,
                Mock.Of<IQuery>(),
                scheduler,
                new MockFileSystem());
            viewModel.ViewInSteamCommand.Execute().Subscribe();
            
            process.Verify(p => p.Start(It.IsAny<ProcessStartInfo>()));
        });

        [Test]
        public Task when_fetching_users_items_queires_steam() => new TestScheduler().WithAsync(async scheduler =>
        {
            var item = new Mock<IItem>();
            
            var query = new Mock<IQuery>();
            query.Setup(q => q.WhereUserPublished(It.IsAny<SteamId>())).Returns(query.Object);
            query.Setup(q => q.WithDifferentApp(It.IsAny<AppId>(), It.IsAny<AppId>())).Returns(query.Object);
            query.Setup(q => q.WithMetadata(It.IsAny<bool>())).Returns(query.Object);
            query.Setup(q => q.GetPageAsync(1)).ReturnsAsync(new ResultsPage(1, new []{ item.Object }));
            query.Setup(q => q.GetPageAsync(2)).ReturnsAsync(new ResultsPage(0, Enumerable.Empty<IItem>()));
            query.Setup(q => q.WithLongDescription(It.IsAny<bool>())).Returns(query.Object);
            
            var viewModel = new HeaderViewModel(Mock.Of<IContext>(),
                Mock.Of<IProcess>(),
                query.Object,
                scheduler,
                new MockFileSystem());

            await viewModel.FetchUsersItems();
            viewModel.CreatedItems.Count.Should().Be(1);
            viewModel.CreatedItems.Should().ContainSingle(i => i.Equals(item.Object));
            
            query.Verify(v => v.GetPageAsync(1), Times.Once);
            query.Verify(v => v.GetPageAsync(2), Times.Once);
        });
        
        [Test, Timeout(60000)]
        public Task when_fetching_users_items_does_not_loop_forever() => new TestScheduler().WithAsync(async scheduler =>
        {
            var item = new Mock<IItem>();
            
            var query = new Mock<IQuery>();
            query.Setup(q => q.WhereUserPublished(It.IsAny<SteamId>())).Returns(query.Object);
            query.Setup(q => q.WithDifferentApp(It.IsAny<AppId>(), It.IsAny<AppId>())).Returns(query.Object);
            query.Setup(q => q.WithMetadata(It.IsAny<bool>())).Returns(query.Object);
            query.Setup(q => q.GetPageAsync(It.IsAny<int>())).ReturnsAsync(new ResultsPage(1, new []{ item.Object }));
            query.Setup(q => q.WithLongDescription(It.IsAny<bool>())).Returns(query.Object);
            
            var viewModel = new HeaderViewModel(Mock.Of<IContext>(),
                Mock.Of<IProcess>(),
                query.Object,
                scheduler,
                new MockFileSystem());

            await viewModel.FetchUsersItems();
        });

        [Test]
        public void when_opening_steam_item_changes_context() => new TestScheduler().With(scheduler =>
        {
            var context = new Mock<IContext>();
            var item = new Mock<IItem>();
            var viewModel = new HeaderViewModel(context.Object,
                Mock.Of<IProcess>(),
                Mock.Of<IQuery>(),
                scheduler,
                new MockFileSystem());

            viewModel.OpenSteamItemCommand.Execute(item.Object).Subscribe();

            context.Verify(c => c.ChangeSteamItemScope(viewModel, item.Object));
        });
    }
}