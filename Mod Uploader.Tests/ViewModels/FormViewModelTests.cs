﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facepunch.Steamworks.Abstractions;
using FluentAssertions;
using Microsoft.Reactive.Testing;
using Mod_Uploader.Abstractions;
using Mod_Uploader.ViewModels;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using ReactiveUI.Testing;
using SteamQueries.Models;
using Steamworks;
using Steamworks.Ugc;

namespace Mod_Uploader.Tests.ViewModels;

public class FormViewModelTests
{
    const string _folderPath = @"C:\CodeFiles\Mod";
    const string _dllName = "SomeDll.dll";
    
    [Category("Unit")]
    public class a_form_view_model
    {
        [Test, Timeout(1000)]
        public Task when_pressing_folder_button_dialog_is_opened() => new TestScheduler().WithAsync(async scheduler =>
        {
            const string folder = @"C:\My Mods\My Amazing Mod";
            var fileSystem = new MockFileSystem();
            fileSystem.AddDirectory(folder);
            fileSystem.AddFile(Path.Combine(folder, "Code.dll"), string.Empty);
            fileSystem.AddFile(Path.Combine(folder, "assets.assetbundle"), string.Empty);
            fileSystem.AddFile(Path.Combine(folder, "thumbnail.png"), string.Empty);
            
            var viewModel = new FormViewModel(Mock.Of<IContext>(), fileSystem, Mock.Of<IEditorFactory>(), Mock.Of<IMD5>(), Mock.Of<IPopUpView>(), Mock.Of<IProcess>());

            var source = new TaskCompletionSource<bool>();
            viewModel.ShowSelectFolderDialog.RegisterHandler(context =>
            {
                source.TrySetResult(true);
                context.SetOutput(folder);
            });
            
            viewModel.SelectFolderCommand.Execute().Subscribe();

            await source.Task;

            viewModel.FolderPath.Should().BeEquivalentTo(folder);
            viewModel.Files.Count.Should().Be(4);
        });
        
        [Test, Timeout(1000)]
        public Task when_pressing_thumbnail_button_dialog_is_opened() => new TestScheduler().WithAsync(async scheduler =>
        {
            const string folder = @"C:\My Mods\My Amazing Mod";
            var imagePath = Path.Combine(folder, "thumbnail.png");
            
            var fileSystem = new MockFileSystem();
            fileSystem.AddDirectory(folder);
            fileSystem.AddFile(Path.Combine(folder, "Code.dll"), string.Empty);
            fileSystem.AddFile(Path.Combine(folder, "assets.assetbundle"), string.Empty);
            fileSystem.AddFile(imagePath, string.Empty);
            
            var viewModel = new FormViewModel(Mock.Of<IContext>(), fileSystem, Mock.Of<IEditorFactory>(), Mock.Of<IMD5>(), Mock.Of<IPopUpView>(), Mock.Of<IProcess>());

            var source = new TaskCompletionSource<bool>();
            viewModel.ShowSelectThumbnailDialog.RegisterHandler(context =>
            {
                source.TrySetResult(true);
                context.SetOutput(imagePath);
            });
            
            viewModel.SelectThumbnailCommand.Execute().Subscribe();

            await source.Task;

            viewModel.ImagePath.Should().BeEquivalentTo(imagePath);
        });

        [Test]
        public Task if_form_is_valid_upload_button_can_be_pressed() => new TestScheduler().WithAsync(async scheduler =>
        {
            const string folder = @"C:\My Mods\My Amazing Mod";
            var imagePath = Path.Combine(folder, "thumbnail.png");
            
            var fileSystem = new MockFileSystem();
            fileSystem.AddDirectory(folder);
            fileSystem.AddFile(Path.Combine(folder, "Code.dll"), string.Empty);
            fileSystem.AddFile(Path.Combine(folder, "assets.assetbundle"), string.Empty);
            fileSystem.AddFile(imagePath, string.Empty);

            var viewModel = new FormViewModel(Mock.Of<IContext>(), fileSystem, Mock.Of<IEditorFactory>(), Mock.Of<IMD5>(), Mock.Of<IPopUpView>(), Mock.Of<IProcess>());

            viewModel.Title = "AnyTitle";
            viewModel.Description = "AnyDescription";
            
            var folderTaskSource = new TaskCompletionSource<bool>();
            var imageTaskSource = new TaskCompletionSource<bool>();
            
            viewModel.ShowSelectFolderDialog.RegisterHandler(context =>
            {
                folderTaskSource.TrySetResult(true);
                context.SetOutput(folder);
            });
            viewModel.ShowSelectThumbnailDialog.RegisterHandler(context =>
            {
                imageTaskSource.TrySetResult(true);
                context.SetOutput(imagePath);
            });
            
            viewModel.SelectThumbnailCommand.Execute().Subscribe();
            viewModel.SelectFolderCommand.Execute().Subscribe();
            await folderTaskSource.Task;
            await imageTaskSource.Task;

            viewModel.CanUpload.Should().BeTrue();
        });

        [Test]
        public void when_selecting_a_thumbnail_needs_to_be_less_than_1mb() => new TestScheduler().WithAsync(async scheduler =>
        {
            const string folder = @"C:\My Mods\My Amazing Mod";
            var imagePath = Path.Combine(folder, "thumbnail.png");
            
            var fileSystem = new MockFileSystem();
            fileSystem.AddDirectory(folder);
            fileSystem.AddFile(Path.Combine(folder, "Code.dll"), string.Empty);
            fileSystem.AddFile(Path.Combine(folder, "assets.assetbundle"), string.Empty);
            fileSystem.AddFile(imagePath, new MockFileData(CreateFileOverBytes(1048576)));
            
            var viewModel = new FormViewModel(Mock.Of<IContext>(), fileSystem, Mock.Of<IEditorFactory>(), Mock.Of<IMD5>(), Mock.Of<IPopUpView>(), Mock.Of<IProcess>());

            var source = new TaskCompletionSource<bool>();
            viewModel.ShowSelectThumbnailDialog.RegisterHandler(context =>
            {
                source.TrySetResult(true);
                context.SetOutput(imagePath);
            });
            
            viewModel.SelectThumbnailCommand.Execute().Subscribe();

            await source.Task;

            viewModel.ImagePathIsInvalid.Should().BeTrue();
            viewModel.ImagePath.Should().BeEmpty();
        });

        [Test]
        public void on_steam_scope_changed_updates_fields()
        {
            const string title = "AnyTitle";
            const string description = "AnyDescription";

            var metaData = JsonConvert.SerializeObject(new ItemMetaData
            {
                AllowLoadOnStart = false, ShowOnMainList = true
            });
            
            var context = new Mock<IContext>();
            var item = new Mock<IItem>();
            item.Setup(i => i.Title).Returns(title);
            item.Setup(i => i.Description).Returns(description);
            item.Setup(i => i.MetaData).Returns(metaData);
            
            var viewModel = new FormViewModel(context.Object, new MockFileSystem(), Mock.Of<IEditorFactory>(), Mock.Of<IMD5>(), Mock.Of<IPopUpView>(), Mock.Of<IProcess>());
            
            context.Raise(c => c.SteamItemScopeChanged += null, this, item.Object);

            viewModel.Title.Should().Be(title);
            viewModel.Description.Should().Be(description);
            viewModel.AllowLoadOnStart.Should().BeFalse();
            viewModel.ShowOnMainMenu.Should().BeTrue();
            
            context.Raise(c => c.SteamItemScopeChanged += null, this, null);
            
            viewModel.Title.Should().BeEmpty();
            viewModel.Description.Should().BeEmpty();
        }

        [Category("Unit")]
        class when_submitting
        {
            [Category("Unit")]
            class a_new_item
            {
                [Test]
                public void and_needs_workshop_agreement_shows_correct_pop_up() => new TestScheduler().With(scheduler =>
                {
                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { NeedsWorkshopAgreement = true, Result = Result.Fail });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(WorkshopFileType.Community)).Returns(editor.Object);

                    var popup = new Mock<IPopUpView>();

                    var viewModel = new FormViewModel(Mock.Of<IContext>(), Mock.Of<IFileSystem>(),
                        editorFactory.Object, Mock.Of<IMD5>(), popup.Object, Mock.Of<IProcess>());

                    viewModel.UploadCommand.Execute().Subscribe();

                    popup.Verify(p => p.ShowPopUp("Failed",
                        "You need to agree to steams workshops terms of service",
                        It.IsAny<string?>(),
                        It.IsAny<string?>()));
                });

                [Test]
                public void and_fails_for_other_reasons_shows_pop_up_with_reason([Values] Result result)
                {
                    // Okay shouldn't fail but there is no way to exclude an enum like this in NUnit
                    if (result.Equals(Result.OK))
                        return;

                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { Result = result });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(WorkshopFileType.Community)).Returns(editor.Object);

                    var popup = new Mock<IPopUpView>();

                    var viewModel = new FormViewModel(Mock.Of<IContext>(), Mock.Of<IFileSystem>(),
                        editorFactory.Object, Mock.Of<IMD5>(), popup.Object, Mock.Of<IProcess>());

                    viewModel.UploadCommand.Execute().Subscribe();

                    popup.Verify(p => p.ShowPopUp("Failed",
                        $"Reason:{result}",
                        It.IsAny<string?>(),
                        It.IsAny<string?>()));
                }

                [Test]
                public void and_successful_uploads_shows_pop_up_to_open()
                {
                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { Result = Result.OK });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(WorkshopFileType.Community)).Returns(editor.Object);

                    var popup = new Mock<IPopUpView>();

                    var viewModel = new FormViewModel(Mock.Of<IContext>(), Mock.Of<IFileSystem>(),
                        editorFactory.Object, Mock.Of<IMD5>(), popup.Object, Mock.Of<IProcess>());

                    viewModel.UploadCommand.Execute().Subscribe();

                    popup.Verify(p => p.ShowPopUp("Uploaded!",
                        "Your item has been uploaded to the steam workshop",
                        "View In Steam",
                        "Okay"));
                }

                [Test]
                public void and_does_not_have_a_dll_selected_submits_fine() => new TestScheduler().With(_ =>
                {
                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { Result = Result.OK });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(WorkshopFileType.Community)).Returns(editor.Object);

                    var viewModel = new FormViewModel(Mock.Of<IContext>(), Mock.Of<IFileSystem>(),
                        editorFactory.Object, Mock.Of<IMD5>(), Mock.Of<IPopUpView>(), Mock.Of<IProcess>());

                    viewModel.UploadCommand.Execute().Subscribe();
                });
            }

            [Category("Unit")]
            class an_existing_item
            {
                [Test]
                public void and_needs_workshop_agreement_shows_correct_pop_up() => new TestScheduler().With(scheduler =>
                {
                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { NeedsWorkshopAgreement = true, Result = Result.Fail });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(It.IsAny<IPublishedFieldId>())).Returns(editor.Object);

                    var popup = new Mock<IPopUpView>();

                    var item = new Mock<IItem>();
                    item.Setup(i => i.Title).Returns("Title");
                    item.Setup(i => i.Description).Returns("Description");
                    item.Setup(i => i.MetaData).Returns(JsonConvert.SerializeObject(new ItemMetaData
                    {
                        AllowLoadOnStart = true,
                        ShowOnMainList = true,
                        DllHash = "SomeHash",
                        DllName = _dllName
                    }));


                    var context = new Mock<IContext>();
                    context.Setup(c => c.GetSteamItemScope()).Returns(item.Object);
                    
                    var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
                    {
                        { Path.Combine(_folderPath, _dllName), new MockFileData("1337") }
                    });

                    var viewModel = new FormViewModel(context.Object, fileSystem,
                        editorFactory.Object, Mock.Of<IMD5>(), popup.Object, Mock.Of<IProcess>());
                    
                    context.Raise(c => c.SteamItemScopeChanged += null, this, item.Object);
                    
                    viewModel.UploadCommand.Execute().Subscribe();

                    popup.Verify(p => p.ShowPopUp("Failed",
                        "You need to agree to steams workshops terms of service",
                        It.IsAny<string?>(),
                        It.IsAny<string?>()));
                });

                [Test]
                public void and_fails_for_other_reasons_shows_pop_up_with_reason([Values] Result result)
                {
                    // Okay shouldn't fail but there is no way to exclude an enum like this in NUnit
                    if (result.Equals(Result.OK))
                        return;

                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { Result = result });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(It.IsAny<IPublishedFieldId>())).Returns(editor.Object);

                    var popup = new Mock<IPopUpView>();

                    var item = new Mock<IItem>();
                    item.Setup(i => i.Title).Returns("Title");
                    item.Setup(i => i.Description).Returns("Description");
                    item.Setup(i => i.MetaData).Returns(JsonConvert.SerializeObject(new ItemMetaData
                    {
                        AllowLoadOnStart = true,
                        ShowOnMainList = true,
                        DllHash = "SomeHash",
                        DllName = _dllName
                    }));


                    var context = new Mock<IContext>();
                    context.Setup(c => c.GetSteamItemScope()).Returns(item.Object);
                    var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
                    {
                        { Path.Combine(_folderPath, _dllName), new MockFileData("1337") }
                    });

                    var viewModel = new FormViewModel(context.Object, fileSystem,
                        editorFactory.Object, Mock.Of<IMD5>(), popup.Object, Mock.Of<IProcess>());
                    
                    context.Raise(c => c.SteamItemScopeChanged += null, this, item.Object);

                    viewModel.UploadCommand.Execute().Subscribe();

                    popup.Verify(p => p.ShowPopUp("Failed",
                        $"Reason:{result}",
                        It.IsAny<string?>(),
                        It.IsAny<string?>()));
                }

                [Test]
                public void and_successful_uploads_shows_pop_up_to_open()
                {
                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { Result = Result.OK });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(It.IsAny<IPublishedFieldId>())).Returns(editor.Object);

                    var popup = new Mock<IPopUpView>();

                    var item = new Mock<IItem>();
                    item.Setup(i => i.Title).Returns("Title");
                    item.Setup(i => i.Description).Returns("Description");
                    item.Setup(i => i.MetaData).Returns(JsonConvert.SerializeObject(new ItemMetaData
                    {
                        AllowLoadOnStart = true,
                        ShowOnMainList = true,
                        DllHash = "SomeHash",
                        DllName = _dllName
                    }));

                    var context = new Mock<IContext>();
                    context.Setup(c => c.GetSteamItemScope()).Returns(item.Object);
                    
                    var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
                    {
                        { Path.Combine(_folderPath, _dllName), new MockFileData("1337") }
                    });

                    var viewModel = new FormViewModel(context.Object, fileSystem,
                        editorFactory.Object, Mock.Of<IMD5>(), popup.Object, Mock.Of<IProcess>());
                    viewModel.FolderPath = _folderPath;
                    
                    context.Raise(c => c.SteamItemScopeChanged += null, this, item.Object);

                    viewModel.UploadCommand.Execute().Subscribe();

                    popup.Verify(p => p.ShowPopUp("Uploaded!",
                        "Your item has been updated",
                        "View In Steam",
                        "Okay"));
                }

                [Test]
                public void and_does_not_have_a_dll_selected_submits_fine() => new TestScheduler().With(_ =>
                {
                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { Result = Result.OK });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(It.IsAny<IPublishedFieldId>())).Returns(editor.Object);

                    var item = new Mock<IItem>();
                    item.Setup(i => i.Title).Returns("Title");
                    item.Setup(i => i.Description).Returns("Description");
                    item.Setup(i => i.MetaData).Returns(JsonConvert.SerializeObject(new ItemMetaData
                    {
                        AllowLoadOnStart = true,
                        ShowOnMainList = true
                    }));

                    var context = new Mock<IContext>();
                    context.Setup(c => c.GetSteamItemScope()).Returns(item.Object);

                    var fileSystem = new MockFileSystem();

                    var viewModel = new FormViewModel(context.Object, fileSystem,
                        editorFactory.Object, Mock.Of<IMD5>(), Mock.Of<IPopUpView>(), Mock.Of<IProcess>());
                    viewModel.FolderPath = _folderPath;
                    
                    context.Raise(c => c.SteamItemScopeChanged += null, this, item.Object);

                    viewModel.UploadCommand.Execute().Subscribe();
                });
                
                [Test]
                public void and_has_dll_selected_with_no_change() => new TestScheduler().With(_ =>
                {
                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { Result = Result.OK });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(It.IsAny<IPublishedFieldId>())).Returns(editor.Object);

                    var item = new Mock<IItem>();
                    item.Setup(i => i.Title).Returns("Title");
                    item.Setup(i => i.Description).Returns("Description");
                    item.Setup(i => i.MetaData).Returns(JsonConvert.SerializeObject(new ItemMetaData
                    {
                        AllowLoadOnStart = true,
                        ShowOnMainList = true,
                        DllName = "AnyDll.dll",
                        DllHash = "AnyHash"
                    }));

                    var context = new Mock<IContext>();
                    context.Setup(c => c.GetSteamItemScope()).Returns(item.Object);

                    var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
                    {
                        { Path.Combine(_folderPath, "AnyDll.dll"), new MockFileData("SomeCode") }
                    });

                    var viewModel = new FormViewModel(context.Object, fileSystem,
                        editorFactory.Object, Mock.Of<IMD5>(), Mock.Of<IPopUpView>(), Mock.Of<IProcess>());
                    
                    context.Raise(c => c.SteamItemScopeChanged += null, this, item.Object);
                    viewModel.FolderPath = _folderPath;
                    
                    viewModel.UploadCommand.Execute().Subscribe();
                    editor.Verify(e => e.WithMetaData(It.IsAny<string>()));
                });

                [Test]
                public void keeps_the_same_tags()
                {
                    
                    var editor = new Mock<IEditor>();
                    editor.Setup(e => e.ForAppId(It.IsAny<AppId>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithTitle(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithDescription(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithMetaData(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithContent(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.WithPreviewFile(It.IsAny<string>()))
                        .Returns(editor.Object);
                    editor.Setup(e => e.SubmitAsync(null, null))
                        .ReturnsAsync(new PublishResult { Result = Result.OK });


                    var editorFactory = new Mock<IEditorFactory>();
                    editorFactory.Setup(f => f.New(It.IsAny<IPublishedFieldId>())).Returns(editor.Object);

                    var popup = new Mock<IPopUpView>();

                    var item = new Mock<IItem>();
                    item.Setup(i => i.Title).Returns("Title");
                    item.Setup(i => i.Description).Returns("Description");
                    item.Setup(i => i.MetaData).Returns(JsonConvert.SerializeObject(new ItemMetaData
                    {
                        AllowLoadOnStart = true,
                        ShowOnMainList = true,
                        DllHash = "SomeHash",
                        DllName = _dllName
                    }));
                    item.Setup(i => i.Tags).Returns(new [] { "Weapon" });

                    var context = new Mock<IContext>();
                    context.Setup(c => c.GetSteamItemScope()).Returns(item.Object);
                    
                    var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
                    {
                        { Path.Combine(_folderPath, _dllName), new MockFileData("1337") }
                    });

                    var viewModel = new FormViewModel(context.Object, fileSystem,
                        editorFactory.Object, Mock.Of<IMD5>(), popup.Object, Mock.Of<IProcess>());
                    viewModel.FolderPath = _folderPath;
                    
                    context.Raise(c => c.SteamItemScopeChanged += null, this, item.Object);

                    viewModel.UploadCommand.Execute().Subscribe();
                    
                    editor.Verify(e => e.WithTag("Weapon"));
                }
            }
        }

        private static string CreateFileOverBytes(long fileSizeInBytes)
        {
            long currentSize = 0;
            var random = new Random();
            var stringBuilder = new StringBuilder();
            
        
            while (currentSize < fileSizeInBytes)
            {
                var randomText = GenerateRandomText(random);
                stringBuilder.Append(randomText);
                currentSize += Encoding.UTF8.GetByteCount(randomText + Environment.NewLine);
            }

            return stringBuilder.ToString();
        }
        
        private static string GenerateRandomText(Random random)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, random.Next(10, 100))
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}